#include <iostream>
#include <fstream>
#include <vector>
using namespace std;

#define RESET "\033[0m" // reset all attributes
#define BLUE_BG "\033[44m" // blue background
#define RED_BG "\033[41m" // red background
#define WHITE_BG "\033[47m" // white background
#define RED_TEXT "\033[31m" // red text

void getDataFromFile(string filename, vector<string>& flag) {
    ifstream file;
    file.open(filename, ios::in);

    if (file.is_open()) {
        string line;

        while(getline(file, line)) {
            flag.push_back(line);
        }

        file.close();
    }
}

void printFlag(vector<string>& flag) {
    for (int i = 0; i < flag.size(); i++) {
        for (int j = 0; j < flag[i].length(); j++) {
            if (i == 0) {
                cout << flag[i][j];
                continue;
            }

            if (flag[i][j] == '*' || flag[i][j] == ' ') {
                cout << BLUE_BG;
            } else if (flag[i][j] == '_') {
                if (i % 2 == 0) {
                    cout << WHITE_BG;
                } else {
                    cout << RED_BG;
                    if (i < flag.size() - 1) {
                        cout << RED_TEXT;
                    }
                }
            }

            cout << flag[i][j];
            cout << RESET;
        }
        cout << endl;
    }
}

int main() {

    vector<string> flag;
    getDataFromFile("flag.txt", flag);
    printFlag(flag);

    return 0;
}
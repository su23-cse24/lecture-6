#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <sstream>
#include <iomanip>
using namespace std;

void readDataFromFile(string filename, vector<vector<string>>& data) {
    ifstream file;
    file.open(filename, ios::in);

    if (file.is_open()) {
        string line;

        while (getline(file, line)) {
            stringstream ss(line);
            string word;
            vector<string> inner;

            while (getline(ss, word, ',')) {
                inner.push_back(word);
            }
            data.push_back(inner);
        }

        file.close();
    }
}

void printTable(vector<vector<string>>& data) {
    for (int i = 0; i < data.size(); i++) {
        if (i == 0 || i == 1) {
            cout << "====================================================================================================" << endl;
        }

        for (int j = 0; j < data[i].size(); j++) {
            cout << setw(25);
            if (j == data[i].size() - 1) {
                cout << std::right;
            } else {
                cout << std::left;
            }
            cout << data[i][j];
        }
        cout << endl;
    }

}

int main() {

    // [ [Full Name, Email] ]

    vector<vector<string>> data;
    readDataFromFile("data2.csv", data);
    printTable(data);

    return 0;
}
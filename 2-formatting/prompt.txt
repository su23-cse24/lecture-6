Write a C++ program that reads the content from csv file named `data.csv`.

The contents of the `data.csv`:
Full Name,Email
Dougie Lazonby,dlazonby0@ucoz.com
Kerrill Peyes,kpeyes1@usnews.com
Correy Dugald,cdugald2@army.mil
Adora Toward,atoward3@globo.com
Marni Cosh,mcosh2@imdb.com
Byrann Shrimptone,bshrimptone5@is.gd
Garald Lanchbery,glanchbery6@usa.gov
Lissy Eamer,leamer7@ucoz.com
Roosevelt Blaes,rblaes3@jimdo.com
Tye Cowlard,tcowlard4@163.com

After reading the contents of `data.csv`, your program should print the
data in the following format:

========================================
Full Name           Email
========================================
Dougie Lazonby      dlazonby0@ucoz.com
Kerrill Peyes       kpeyes1@usnews.com
Correy Dugald       cdugald2@army.mil
Adora Toward        atoward3@globo.com
Marni Cosh          mcosh2@imdb.com
Byrann Shrimptone   bshrimptone5@is.gd
Garald Lanchbery    glanchbery6@usa.gov
Lissy Eamer         leamer7@ucoz.com
Roosevelt Blaes     rblaes3@jimdo.com
Tye Cowlard         tcowlard4@163.com